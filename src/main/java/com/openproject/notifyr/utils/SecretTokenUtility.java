package com.openproject.notifyr.utils;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public final class SecretTokenUtility {

    private SecretTokenUtility() {}

    public static String generateSecretToken() {
        return getRandom(1, 10) + lastSixDigitsTime() + getRandom(1, 10);
    }

    private static String lastSixDigitsTime() {
        long seconds = System.currentTimeMillis() / 1000L;
        String lastSixDigits = Long.toString(seconds);
        return lastSixDigits.substring(lastSixDigits.length() - 6);
    }

    private static String getRandom(int min, int max) {
        int randomNum = new Random().nextInt(max - min) + min;
        return Integer.toString(randomNum);
    }

}
