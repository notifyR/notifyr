package com.openproject.notifyr.dao;

public interface ValidateTokenDao {

    public Integer validateAndGetClientId(String event,String authToken);
    public Integer validateAndGetContractId(String event,String authToken);
}
