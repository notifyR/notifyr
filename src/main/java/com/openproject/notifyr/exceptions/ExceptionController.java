package com.openproject.notifyr.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ExceptionController {

    public <T> Map<String,Object> getServerErrorMessage(T exception){
        String message = null;
        if(exception instanceof SettingsException)
            message=((SettingsException) exception).getLocalizedMessage();

        Map<String, Object> map = new HashMap<>();
        map.put("isSuccess", false);
        map.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
        map.put("message", message);
        map.put("timeStamp", new Date().toString());
        return map;
    }

    public <T> Map<String,Object> getUnauthorizedMessage(T exception){
        String message = null;
        if(exception instanceof UnauthorizedException)
            message=((UnauthorizedException) exception).getLocalizedMessage();

        Map<String, Object> map = new HashMap<>();
        map.put("isSuccess", false);
        map.put("status", HttpStatus.UNAUTHORIZED);
        map.put("message", message);
        map.put("timeStamp", new Date().toString());
        return map;
    }

    @ExceptionHandler(value = UnauthorizedException.class)
    public ResponseEntity<Object> unauthorizedException(UnauthorizedException unauthorizedException){
        return new ResponseEntity<>(this.getUnauthorizedMessage(unauthorizedException), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = SettingsException.class)
    public ResponseEntity<Object> settingsException(SettingsException settingsException){
        return new ResponseEntity<>(this.getServerErrorMessage(settingsException),HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
