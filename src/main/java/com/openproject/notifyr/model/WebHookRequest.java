package com.openproject.notifyr.model;

import javax.validation.constraints.NotNull;


public class WebHookRequest {
    @NotNull
    private int clientId;
    @NotNull
    private String carrier;
    @NotNull
    private String message;

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "WebHookRequest{" +
                "clientId=" + clientId +
                ", carrier='" + carrier + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
