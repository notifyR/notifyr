package com.openproject.notifyr.exceptions;

public class SettingsException extends  RuntimeException{
    public SettingsException(String message){
        super(message);
    }
}
