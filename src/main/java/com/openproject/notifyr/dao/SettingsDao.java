package com.openproject.notifyr.dao;

import com.openproject.notifyr.model.Mail;

public interface SettingsDao {
    public Mail getCarrierSettings(Integer contractId,String carrierType);
    public String storeCarrierSettings(Integer clientId,String carrierType,String mailObj);
}
