package com.openproject.notifyr.dao;

public interface TokenDao {
    public Boolean storeToken(Integer clientId, String carrierName, String token, String validity);
}
