package com.openproject.notifyr.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.openproject.notifyr.exceptions.SettingsException;
import com.openproject.notifyr.model.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.*;
import org.springframework.stereotype.Repository;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import static com.openproject.notifyr.dao.QueryUtil.CONTRACT_INSERT_QUERY;
import static com.openproject.notifyr.dao.QueryUtil.GET_CONTRACT_QUERY;
import static com.openproject.notifyr.utils.Security.sha256;

@Repository
public class SettingsDaoImpl implements SettingsDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public Mail getCarrierSettings(Integer contractId,String carrierType){
        return jdbcTemplate.query(GET_CONTRACT_QUERY, new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps) throws SQLException {
                ps.setInt(1,contractId);
            }
        }, new ResultSetExtractor<Mail>() {
            @Override
            public Mail extractData(ResultSet rs) throws SQLException, DataAccessException {
                String jsonMailObj = "";
                Mail mail = new Mail();
                if (rs.next())
                    jsonMailObj = rs.getString(1);
                ObjectMapper objectMapper = new ObjectMapper();
                try {
                    mail = objectMapper.readValue(jsonMailObj, Mail.class);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }

                return mail;
            }
        });
    }

    public String storeCarrierSettings(Integer clientId,String carrierType,String carrierObjStr){
                String authenticationToken = sha256(UUID.randomUUID().toString()).substring(1, 26);
        try {
            jdbcTemplate.execute(CONTRACT_INSERT_QUERY, new PreparedStatementCallback<Boolean>() {

                @Override
                public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                    ps.setInt(1, clientId);
                    ps.setString(2, authenticationToken);
                    ps.setString(3, carrierObjStr);
                    ps.setString(4, carrierType);
                    return ps.execute();
                }
            });
        }catch (Exception exception){
            throw new SettingsException("Unable to store settings and generate a webhook");
        }
            return authenticationToken;

    }

}
