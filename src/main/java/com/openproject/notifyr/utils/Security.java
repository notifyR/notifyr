package com.openproject.notifyr.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.MessageDigest;
import java.sql.SQLOutput;

public final class Security {
    private Security(
    ){}

    private static final String AES_KEY="Bar12345Bar12345";

    public static String sha256(final String base) {
        try{
            final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(base.getBytes("UTF-8"));
            final StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < hash.length; i++) {
                final String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    public static String encryptByAES(String plaintext){
        String cipherText="";
        try {
            Key aesKey = new SecretKeySpec(AES_KEY.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            byte[] utf8 = plaintext.getBytes("UTF8");
            byte[] encrypted = cipher.doFinal(utf8);
            cipherText=new sun.misc.BASE64Encoder().encode(encrypted);
        }catch (Exception encryptionEx){
            System.out.println("encryption issue: "+encryptionEx.getMessage());
        }
        return cipherText;
    }

    public static String decryptByAES(String cipherText){
        String plainText="";
        try {
            Key aesKey = new SecretKeySpec(AES_KEY.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, aesKey);
            //byte[] encrypted=cipherText.getBytes();
            byte[] encryptedStr = new sun.misc.BASE64Decoder().decodeBuffer(cipherText);
            byte[] utf8 = cipher.doFinal(encryptedStr);
            plainText=new String(utf8, "UTF8");
        }catch (Exception decryptionEx){
            System.out.println("Decryption Issue: "+decryptionEx.getMessage());
        }

        return plainText;
    }
}
