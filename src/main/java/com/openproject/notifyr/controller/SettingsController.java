package com.openproject.notifyr.controller;

import com.openproject.notifyr.model.Mail;
import com.openproject.notifyr.model.ResponseBody;
import com.openproject.notifyr.service.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/settings")
public class SettingsController {

    @Autowired
    Settings settings;

    @PostMapping("/webHook/mail/{clientId}")
    public ResponseBody<Object> mailHookSettings(@RequestBody Mail mail, @PathVariable(required = true,name="clientId") Integer clientId){
        Object authToken=(Object) settings.mailHookSettings(clientId,mail);
        return ResponseBody.builder()
                .data(authToken)
                .build();
    }

    @GetMapping("/getToken/telegram/{clientId}")
    public ResponseBody<Object> getToken(@PathVariable(required = true, name = "clientId") Integer clientId) {
        String token = settings.getSecretToken("TELEGRAM", clientId);
        return ResponseBody.<Object>builder()
                .data(token)
                .status(HttpStatus.OK)
                .build();
    }


}
