package com.openproject.notifyr.model;

import javax.validation.constraints.NotNull;

public class Mail {

    @NotNull
    private String to;
    @NotNull
    private String from;
    private String content;
    @NotNull
    private String subject;
    private String ccList;
    @NotNull
    private String protocol;
    @NotNull
    private String host;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private int port;
    private Boolean startTls;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCcList() {
        return ccList;
    }

    public void setCcList(String ccList) {
        this.ccList = ccList;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Boolean getStartTls() {
        return startTls;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setStartTls(Boolean startTls) {
        this.startTls = startTls;
    }

    @Override
    public String toString() {
        return "Mail{" +
                "to='" + to + '\'' +
                ", from='" + from + '\'' +
                ", content='" + content + '\'' +
                ", subject='" + subject + '\'' +
                ", ccList='" + ccList + '\'' +
                ", protocol='" + protocol + '\'' +
                ", host='" + host + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", port=" + port +
                ", startTls=" + startTls +
                '}';
    }
}
