package com.openproject.notifyr.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;

@Repository
public class TokenDaoImpl implements TokenDao{

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public Boolean storeToken(Integer clientId, String carrierName, String token, String validity) {
        return jdbcTemplate.execute(QueryUtil.OTP_INSERT_QUERY, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                ps.setInt(1, clientId);
                ps.setString(2, token);
                ps.setString(3, validity);
                ps.setString(4, carrierName);

                return ps.executeUpdate() == 1;
            }
        });
    }
}
