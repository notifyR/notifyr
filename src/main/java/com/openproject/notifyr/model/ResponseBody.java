package com.openproject.notifyr.model;

import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Builder
@Getter
public class ResponseBody<T> {

    @Builder.Default
    HttpStatus status=HttpStatus.OK;
    T data;

}
