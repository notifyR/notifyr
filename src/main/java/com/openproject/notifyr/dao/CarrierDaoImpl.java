package com.openproject.notifyr.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class CarrierDaoImpl implements CarrierDao{

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public Integer getCarrierId(String carrierName) {
        return jdbcTemplate.query(QueryUtil.GET_CARRIER_ID, new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps) throws SQLException {
                ps.setString(1, carrierName);
            }
        }, new ResultSetExtractor<Integer>() {
            @Override
            public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
                if(rs.next())
                    return rs.getInt("id");
                else return 0;
            }
        });
    }
}
