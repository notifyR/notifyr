package com.openproject.notifyr.dao;

public final class QueryUtil {
    private QueryUtil(){}

    public static final String CONTRACT_INSERT_QUERY="insert into contract(client_id,carrier_id,authorization_token,settings) select ?,id,?,? from carrier where name=?";
    public static final String CLIENT_VALIDATION_QUERY_GET_CLIENT_ID="select client_id from contract as t1 inner join carrier as t2 on t1.carrier_id=t2.id where t2.name=? and t1.authorization_token=?;";
    public static final String CLIENT_VALIDATION_QUERY_GET_CONTRACT_ID="select t1.id from contract as t1 inner join carrier as t2 on t1.carrier_id=t2.id where t2.name=? and t1.authorization_token=?;";
    public static final String GET_CONTRACT_QUERY="select settings from contract where id = ?;";
    public static final String GET_CARRIER_ID = "SELECT id FROM carrier WHERE name = ?";
    public static final String OTP_INSERT_QUERY = "INSERT INTO otp (client_id, carrier_id, token, validity) SELECT ?, id, ?, ? FROM carrier WHERE name = ?";

}
