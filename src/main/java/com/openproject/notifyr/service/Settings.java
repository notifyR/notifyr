package com.openproject.notifyr.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.openproject.notifyr.dao.CarrierDao;
import com.openproject.notifyr.dao.SettingsDao;
import com.openproject.notifyr.exceptions.SettingsException;
import com.openproject.notifyr.dao.TokenDao;
import com.openproject.notifyr.model.Mail;
import com.openproject.notifyr.utils.SecretTokenUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import static com.openproject.notifyr.utils.Security.encryptByAES;

@Service
public class Settings {

    @Autowired
    Validate validate;

    @Autowired
    SettingsDao settingsDao;


    @Value("${notifyr.baseUrl}")
    private String baseUrl;

    @Value("${server.servlet.context-path}")
    private String contextRoot;

    @Value("${notifyr.mailWebookPath}")
    private String mailWebhookPath;


    @Autowired
    CarrierDao carrierDao;

    @Autowired
    TokenDao tokenDao;

    public String mailHookSettings(Integer clientId,Mail mail){
        try {
            mail.setPassword(encryptByAES(mail.getPassword()));
            ObjectMapper objectMapper = new ObjectMapper();
            String jsonMailObj = objectMapper.writeValueAsString(mail);
            String authToken= settingsDao.storeCarrierSettings(clientId,"MAIL",jsonMailObj);
            return baseUrl+contextRoot+mailWebhookPath+authToken;
        } catch (JsonProcessingException jsonProcessingException) {
            throw new SettingsException("Unable to store settings and generate a webhook.");
        }
    }

    public String getSecretToken(String event, Integer clientId) {
        String secretToken = SecretTokenUtility.generateSecretToken();
        if(tokenDao.storeToken(clientId, event, secretToken, "ACTIVE")) {
            return secretToken;
        } else {
            return null;
        }
    }

}
