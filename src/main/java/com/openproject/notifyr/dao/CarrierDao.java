package com.openproject.notifyr.dao;

public interface CarrierDao {
    public Integer getCarrierId(String carrierName);
}
