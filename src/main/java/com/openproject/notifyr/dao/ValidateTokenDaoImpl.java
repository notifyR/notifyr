package com.openproject.notifyr.dao;

import com.openproject.notifyr.exceptions.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.openproject.notifyr.dao.QueryUtil.*;

@Repository
public class ValidateTokenDaoImpl implements ValidateTokenDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Integer validateAndGetClientId(String event,String authToken){
        return jdbcTemplate.query(CLIENT_VALIDATION_QUERY_GET_CLIENT_ID, new PreparedStatementSetter(){
            public void setValues(PreparedStatement ps) throws SQLException {
                ps.setString(1,event);
                ps.setString(2,authToken);
            }
        },new ResultSetExtractor<Integer>(){
            public Integer extractData(ResultSet rs) throws SQLException {
                if(rs.next())
                    return rs.getInt("client_id");
                else throw new UnauthorizedException("Invalid URL");
            }
        });
    }

    public Integer validateAndGetContractId(String event,String authToken){
        return jdbcTemplate.query(CLIENT_VALIDATION_QUERY_GET_CONTRACT_ID, new PreparedStatementSetter(){
            public void setValues(PreparedStatement ps) throws SQLException {
                ps.setString(1,event);
                ps.setString(2,authToken);
            }
        },new ResultSetExtractor<Integer>(){
            public Integer extractData(ResultSet rs) throws SQLException {
                if(rs.next())
                    return rs.getInt("id");
                else throw new UnauthorizedException("Invalid URL");
            }
        });
    }
}
