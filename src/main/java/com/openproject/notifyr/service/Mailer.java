package com.openproject.notifyr.service;

import com.openproject.notifyr.dao.SettingsDao;
import com.openproject.notifyr.model.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

import static com.openproject.notifyr.utils.Security.decryptByAES;

@Service
public class Mailer {

    @Autowired
    SettingsDao settingsDao;


    public Mail prepareMailData(Integer contractId){
        return settingsDao.getCarrierSettings(contractId,"MAIL");
    }

    public Boolean sendMail(Mail mail) {
        try {
            Properties properties = new Properties();
            properties.put("mail.smtp.auth", true);
            properties.put("mail.smtp.starttls.enable", mail.getStartTls());
            properties.put("mail.smtp.host", mail.getHost());
            properties.put("mail.smtp.port", mail.getPort());

            Session session = Session.getInstance(properties,new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(mail.getUsername(), decryptByAES(mail.getPassword()));
                }
            });

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mail.getFrom(), false));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getTo(), false));
            message.setSubject(mail.getSubject());
            message.setContent(mail.getContent(), "text/html");
            message.setSentDate(new Date());

            Transport.send(message);
        }catch (AddressException addressException){
            System.out.println("Address issue Exception "+addressException.getMessage());
        }
        catch (MessagingException messagingException){
            System.out.println("Cannot send Mail Exception "+messagingException.getMessage());
        }
        return true;
    }
}
