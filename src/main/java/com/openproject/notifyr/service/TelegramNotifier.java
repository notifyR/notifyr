package com.openproject.notifyr.service;

import com.openproject.notifyr.model.Telegram;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

@Service
public class TelegramNotifier {

    private final RestTemplate restTemplate;
    private String telegramServiceUrl;

    public TelegramNotifier(RestTemplate restTemplate, @Value("${service.telegram.url}") String telegramServiceUrl) {
        this.restTemplate = restTemplate;
        this.telegramServiceUrl = telegramServiceUrl;
    }

    public Telegram prepareTelegramData(String message, String authToken, Integer contractId) {
        // to extract chat id from client table using contract id
        String chatId = "-1001648910470";
        return new Telegram(message, authToken, chatId);
    }

    public void sendTelegramMessage(Telegram telegram) {
        try {
            Map<String, String> urlParams = new HashMap<>();
            urlParams.put("bot_token", telegram.getBotAuthToken());
            urlParams.put("chat_id", telegram.getChatId());
            urlParams.put("message", telegram.getMessage().trim());

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(telegramServiceUrl);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            HttpEntity<?> entity = new HttpEntity<>(headers);
            ResponseEntity<Object> response = restTemplate.exchange(builder.buildAndExpand(urlParams).toUri(), HttpMethod.POST, entity, Object.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
