package com.openproject.notifyr.model;

import javax.validation.constraints.NotNull;

public class Telegram {
    @NotNull
    private String message;

    @NotNull
    private String botAuthToken;

    @NotNull
    private String chatId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBotAuthToken() {
        return botAuthToken;
    }

    public void setBotAuthToken(String botAuthToken) {
        this.botAuthToken = botAuthToken;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    @Override
    public String toString() {
        return "Telegram{" +
                "message='" + message + '\'' +
                ", botAuthToken='" + botAuthToken + '\'' +
                ", chatId='" + chatId + '\'' +
                '}';
    }

    public Telegram(String message, String botAuthToken, String chatId) {
        this.message = message;
        this.botAuthToken = botAuthToken;
        this.chatId = chatId;
    }
}
