package com.openproject.notifyr.service;

import com.openproject.notifyr.dao.ValidateTokenDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Validate {

    @Autowired
    ValidateTokenDao validateToken;

    public Integer validateAuthToken(String event, String authToken){
            return validateToken.validateAndGetContractId(event,authToken);
    }
}
