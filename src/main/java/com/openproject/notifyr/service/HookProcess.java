package com.openproject.notifyr.service;

import com.openproject.notifyr.model.Mail;
import com.openproject.notifyr.model.Telegram;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
@Service
public class HookProcess {

    @Autowired
    Mailer mailer;

    @Autowired
    TelegramNotifier telegramNotifier;

    @Autowired
    Validate validate;

    public ResponseEntity sendMailNotification(String authToken,String message, String event) {
        Integer contractId = validate.validateAuthToken(event, authToken);
        if (!contractId.equals(0)) {
            Mail mail = mailer.prepareMailData(contractId);
            mail.setContent(message);
            mailer.sendMail(mail);
            return new ResponseEntity(HttpStatus.OK);
        } else return new ResponseEntity(HttpStatus.UNAUTHORIZED);
    }

    public ResponseEntity sendTelegramNotification(String authToken, String message, String event) {
        Integer contractId = validate.validateAuthToken(event, authToken);
        if(!contractId.equals(0)) {
            Telegram telegram = telegramNotifier.prepareTelegramData(message, authToken, contractId);
            telegramNotifier.sendTelegramMessage(telegram);
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
    }

}
