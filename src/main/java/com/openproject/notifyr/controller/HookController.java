package com.openproject.notifyr.controller;

import com.openproject.notifyr.service.HookProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/webHook")
public class HookController {

    @Autowired
    HookProcess hookProcess;

    @GetMapping("/mail/{authToken}")
    public ResponseEntity processHook(@PathVariable(required = true,name = "authToken") String authToken,
                                      @RequestParam(required = true,name="message") String message){
        return hookProcess.sendMailNotification(authToken,message, "MAIL");
    }

    @GetMapping("/telegram/{authToken}")
    public ResponseEntity processTelegramHook(@PathVariable(required = true, name = "authToken") String authToken,
                                              @RequestParam(required = true, name = "message") String message) {
        return hookProcess.sendTelegramNotification(authToken, message, "TELEGRAM");
    }
}
